$.validator.addMethod("valueNotEquals", function (value, element, arg) {
    return arg !== value;
}, "Value must not equal arg.");

$("#form_crear_rescatado").validate({
    rules: {
        nombre: {
            required: true
        },
        raza_predominante: {
            valueNotEquals: "default"
        },
        descripcion: {
            required: true
        },
        estado: {
            valueNotEquals: "default"
        },
    },
    messages: {
        nombre: {
            required: "Campo requerido"
        },
        raza_predominante: {
            valueNotEquals: "Campo requerido"
        },
        descripcion: {
            required: "Campo requerido"
        },
        estado: {
            valueNotEquals: "Campo requerido"
        }
    },
});

$("#form_editar_rescatado").validate({
    rules: {
        nombre_editar: {
            required: true
        },
        raza_predominante_editar: {
            valueNotEquals: "default"
        },
        descripcion_editar: {
            required: true
        },
        estado_editar: {
            valueNotEquals: "default"
        },
    },
    messages: {
        nombre_editar: {
            required: "Campo requerido"
        },
        raza_predominante_editar: {
            valueNotEquals: "Campo requerido"
        },
        descripcion_editar: {
            required: "Campo requerido"
        },
        estado_editar: {
            valueNotEquals: "Campo requerido"
        }
    },
});