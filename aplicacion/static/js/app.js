if ('serviceWorker' in navigator) {
    window.addEventListener('load', function () {
        navigator.serviceWorker.register('/sw.js')
            .then(function () {
                console.log('ServiceWorker registered!');
            })
            .catch(function (err) {
                console.log('ServiceWorker failed :(', err);
            });
    });
}
/*
$.ajax({
    url: "/api/computadores/", success: function (result) {
        result.forEach(e => {
            $("#galeria").append(
                '<h4>' + e.nombre + '</h4>' +
                '<img src="' + e.foto + '" alt="" height="200" >' +
                '<h5>' + e.precio + '</h5>' +
                '</div>');
        });
    }
});
*/
function cargarDatosEditar(nombre, raza_predominante, descripcion, estado, id) {
    $("#nombre_editar").val(nombre);
    $("#raza_predominante_editar").val(raza_predominante);
    $("#descripcion_editar").val(descripcion);
    $("#estado_editar").val(estado);
    $("#id_rescatado_editar").val(id);
}