$(document).ready(function () {
    $(".alert").delay(5000).slideUp(2000)
});

$("#form_restablecer_contrasena").validate({
    rules: {
        nombre_usuario: {
            required: true
        },
        fname: {
            required: true
        },
        lname: {
            required: true
        },
        correo: {
            required: true,
            email: true
        },
        nueva_contrasena: {
            required: true
        },
        repetir_nueva_contrasena: {
            equalTo: "#nueva_contrasena"
        },
    },
    messages: {
        nombre_usuario: {
            required: "Campo requerido"
        },
        fname: {
            required: "Campo requerido"
        },
        lname: {
            required: "Campo requerido"
        },
        correo: {
            required: "Campo requerido",
            email: "Correo invalido"
        },
        nueva_contrasena: {
            required: "Campo requerido"
        },
        repetir_nueva_contrasena: {
            equalTo: "Contraseñas no son iguales"
        },
    },
});