from django.db import models
from django import forms
from django.contrib.auth.models import User, Group
from rest_framework import serializers


# Create your models here.


class Rescatado(models.Model):
    fotografia = models.ImageField(upload_to="fotos/")
    nombre = models.CharField(max_length=100)
    raza_predominante = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)

    def __str__(self):
        return "Rescatado"
