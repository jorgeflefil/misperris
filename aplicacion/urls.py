from django.urls import path
from rest_framework import routers
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'rescatados', views.RescatadoViewSet)

urlpatterns = [
    path('', views.index, name="index"),
    # Sesiones
    path('login/', views.login, name="login"),
    path('login_iniciar/', views.login_iniciar, name="login_iniciar"),
    path('cerrar_sesion', views.cerrar_sesion, name="cerrar_sesion"),

    # Registro
    path('signin/', views.signin, name="signin"),
    path('signin/crear_usuario', views.crear_usuario, name="crear_usuario"),

    # Reestablecer contraseña
    path('restablecer_contrasena', views.restablecer_contrasena,
         name="restablecer_contrasena"),
    path('restablecido', views.restablecido, name='restablecido'),
    # Listar Rescatados
    path('rescatados/', views.rescatados, name="rescatados"),

    # Mantenedor de rescatados
    path('mantenedor_admin/', views.mantenedor_admin, name="mantenedor_admin"),
    path('mantenedor_admin/crearRescatado',
         views.crearRescatado, name="crearRescatado"),
    path('mantenedor_admin/eliminarRescatado/<int:id>',
         views.eliminarRescatado, name="eliminarRescatado"),
    path('mantenedor_admin/editar_rescatado',
         views.editar_rescatado, name="editar_rescatado"),
    path('api/', include(router.urls)),

    path('login/login/', views.login, name='login'),
    path('login/logout/', views.logout, name='logout'),
    path('login/auth/', include('social_django.urls', namespace='social')), 
    path('home', views.home, name='home'),
    path('sw.js', (TemplateView.as_view(template_name="sw.js", content_type='application/javascript', )), name='sw.js'),
    path('manifest.json', (TemplateView.as_view(template_name="manifest.json", content_type='application/json', )), name='manifest.json'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
