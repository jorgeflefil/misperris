from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Rescatado

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'first_name', 'last_name')

class RescatadoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rescatado
        fields = ('url', 'fotografia', 'nombre', 'raza_predominante', 'descripcion', 'estado')