from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from .models import Rescatado


# Create your tests here.


class RescatadoTestCase(TestCase):

    def test_crear_rescatado(self):
        """
        Result = Resultado real de la funcion
        Expected = Valor esperado a retornar

        0 = Error 
        1 = Ok
        """

        # Arrange
        nombre_rescatado = 'RescatadoTest'
        raza_predominante = 'RazaTest'
        descripcion_rescatado = 'DescripcionRescatado'
        estado = 'Disponible'
        fotografia = None

        result = 0
        expected = 1

        # Act
        if (estado == 'Disponible' or estado == 'Rescatado' or estado == 'Adoptado'):
            rescatado = Rescatado(nombre=nombre_rescatado, raza_predominante=raza_predominante,
                                  descripcion=descripcion_rescatado, estado=estado, fotografia=fotografia)
            rescatado.save()
            result = 1
        else:
            result = 0

        # Assert
        self.assertEqual(result, expected)

class RestablecerTestCase(TestCase):

    def test_restablecer_contrasena(self):
        #Creacion usuario de testing
        nombre_usuario = 'TestUser'
        email = 'test@domain.cl'
        contrasenia = 'test1234'
        nombre = 'Test'
        apellido = 'User'

        user = User.objects.create_user(username=nombre_usuario, email=email,password=contrasenia, first_name=nombre, last_name=apellido)
        user.save()

        result = 0
        expected = 1

        nueva_contrasena = 'cambiada123'
        try:
            u = User.objects.get(username=nombre_usuario,
                             email__icontains=email, first_name__icontains=nombre, last_name__icontains=apellido)
        except User.DoesNotExist:
            u = None

        if u is not None:
            u.set_password(nueva_contrasena)
            u.save()
            result = 1
        else:
            result = 0
        # Assert
        self.assertEqual(result, expected)