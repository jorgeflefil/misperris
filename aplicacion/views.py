# sistema de autenticación
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout

# Decorators
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

# Importar Redireccionamientos
from django.http import HttpResponse
from django.shortcuts import redirect, render

# Importar Usuarios y grupos
from django.contrib.auth.models import User, Group

# Importar clase Rescatado
from .models import Rescatado

# Rest Framework
from rest_framework import viewsets
from .serializer import UserSerializer
from .serializer import RescatadoSerializer


# Create your views here.

def index(request):
    usuario = request.session.get('usuario', None)
    if usuario is not None:
        user = User.objects.get(username=usuario)
        if user.groups.filter(name='adoptantes').exists():
            grupo = 'adoptantes'
        elif user.groups.filter(name='administradores').exists():
            grupo = 'administradores'
        else:
            grupo = None
        return render(request, 'index.html', {'usuario': usuario, 'galeria': Rescatado.objects.all(), 'grupo': grupo})
    else:
        return render(request, 'index.html', {'galeria': Rescatado.objects.all()})

def home(request):
	return render(request, 'index.html')

def login(request):
    return render(request, 'login.html', {'error': ''})


def login_iniciar(request):
    nombre_usuario = request.POST.get('nombre_usuario', '')
    contrasena = request.POST.get('contrasena', '')

    # Asignar Sesion
    user = authenticate(request, username=nombre_usuario, password=contrasena)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.username
        return redirect('index')
    else:
        return render(request, 'login.html', {'error': 'Usuario o contraseña incorrectos'})


@login_required(login_url='login')
def cerrar_sesion(request):
    logout(request)
    return redirect('index')


# Registrar Usuarios
def signin(request):
    return render(request, 'signin.html', {})


def crear_usuario(request):
    nombre_usuario = request.POST.get('nombre_usuario', '')
    contrasenia = request.POST.get('contrasenia', '')
    email = request.POST.get('email', '')
    nombre = request.POST.get('nombre', '')
    apellido = request.POST.get('apellido', '')
    user = User.objects.create_user(username=nombre_usuario, email=email,
                                    password=contrasenia, first_name=nombre, last_name=apellido)

    grupo_adoptantes = Group.objects.get(name='adoptantes')
    user.groups.add(grupo_adoptantes)
    user.save()

    print('Usuario creado')
    return redirect('login')


def group_check_admin(user):
    if user.groups.filter(name='administradores').exists():
        return True
    else:
        return False


# Mantenedor de Rescatados
@login_required(login_url='login')
#@user_passes_test(group_check_admin, login_url='cerrar_sesion')
def mantenedor_admin(request):
    usuario = request.session.get('usuario', None)
    if usuario is not None:
        user = User.objects.get(username=usuario)

        if user.groups.filter(name='adoptantes').exists():
            grupo = 'adoptantes'
        elif user.groups.filter(name='administradores').exists():
            grupo = 'administradores'
        else:
            grupo = None

        return render(request, 'mantenedor_admin.html', {'usuario': usuario, 'lista_rescatados': Rescatado.objects.all(), 'grupo': grupo})
    else:
        return render(request, 'mantenedor_admin.html', {'usuario': usuario, 'lista_rescatados': Rescatado.objects.all()})


@login_required(login_url='login')
def rescatados(request):
    usuario = request.session.get('usuario', None)
    if usuario is not None:
        user = User.objects.get(username=usuario)

        if user.groups.filter(name='adoptantes').exists():
            grupo = 'adoptantes'
        elif user.groups.filter(name='administradores').exists():
            grupo = 'administradores'
        else:
            grupo = None

        return render(request, 'rescatados.html', {'usuario': usuario, 'lista_rescatados': Rescatado.objects.all(), 'grupo': grupo})
    else:
        return render(request, 'rescatados.html', {'usuario': usuario, 'lista_rescatados': Rescatado.objects.all()})


@login_required(login_url='login')
#@user_passes_test(group_check_admin, login_url='cerrar_sesion')
def crearRescatado(request):
    nombre = request.POST.get('nombre', '')
    raza_predominante = request.POST.get('raza_predominante', '')
    descripcion = request.POST.get('descripcion', '')
    estado = request.POST.get('estado', '')
    fotografia = request.FILES['foto']

    rescatado = Rescatado(nombre=nombre, raza_predominante=raza_predominante,
                          descripcion=descripcion, estado=estado, fotografia=fotografia)
    rescatado.save()
    return redirect('mantenedor_admin')


@login_required(login_url='login')
#@user_passes_test(group_check_admin, login_url='cerrar_sesion')
def eliminarRescatado(request, id):
    rescatado = Rescatado.objects.get(pk=id)
    rescatado.delete()
    return redirect('mantenedor_admin')


@login_required(login_url='login')
#@user_passes_test(group_check_admin, login_url='cerrar_sesion')
def editar_rescatado(request):
    nombre = request.POST.get('nombre_editar', '')
    raza_predominante = request.POST.get('raza_predominante_editar', '')
    descripcion = request.POST.get('descripcion_editar', '')
    estado = request.POST.get('estado_editar', '')
    id = request.POST.get('id_rescatado_editar', 0)
    rescatado = Rescatado.objects.get(pk=id)
    rescatado.nombre = nombre
    rescatado.raza_predominante = raza_predominante
    rescatado.descripcion = descripcion
    rescatado.estado = estado
    fotografia = request.FILES.get('foto_editar', False)
    if fotografia is not False:
        rescatado.fotografia = fotografia
    rescatado.save()
    return redirect('mantenedor_admin')


def restablecer_contrasena(request):
    return render(request, 'restablecer_contrasena.html', {})


def restablecido(request):
    nombre_usuario = request.POST.get('nombre_usuario', '')
    fname = request.POST.get('fname', '')
    lname = request.POST.get('lname', '')
    correo = request.POST.get('correo', '')
    nueva_contrasena = request.POST.get('nueva_contrasena', '')
    repetir_nueva_contrasena = request.POST.get('repetir_nueva_contrasena', '')
    try:
        u = User.objects.get(username=nombre_usuario,
                             email__icontains=correo, first_name__icontains=fname, last_name__icontains=lname)
    except User.DoesNotExist:
        u = None

    if u is not None:
        print("Se han cambiado los datos del usuario " +
              str(u.pk) + "-" + u.first_name)
        u.set_password(nueva_contrasena)
        u.save()
        respuesta = 1
    else:
        respuesta = 0

    return render(request, 'restablecer_contrasena.html', {'respuesta': respuesta})


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class RescatadoViewSet(viewsets.ModelViewSet):
    queryset = Rescatado.objects.all()
    serializer_class = RescatadoSerializer
